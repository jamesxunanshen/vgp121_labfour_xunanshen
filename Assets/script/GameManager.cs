﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    //Ensure only one copy of the class. 
    //creates a class variable to keep track of the instance
    //defaulted to private

    static GameManager _instance= null;

    //Use a prefab to create Character Instance
   //public GameObject playerPrefab;

    // Use this for initialization
    void Start()
    {
        if (instance)
        {
            //destroy second copy of gamemanger
            DestroyImmediate(gameObject);
        }
        else
        {
            //assigns gamemanager to variable _instance.
            instance = this;

            //does not destroy when scene switches.

            DontDestroyOnLoad(this);

        }

        score = 0;
        life = 3;
    }

    // Update is called once per frame
    void Update()
    {


        if (Input.GetKeyDown(KeyCode.Escape))
        {
            //can type out name of scene or by index.


             if (SceneManager.GetActiveScene().name == "GameOver")
            {
                SceneManager.LoadScene(0);
            }

        }
        if (life <=0)
        {
            SceneManager.LoadScene(2);
            life = 3;
            score = 0;
        }

                
    }

    /*  public void spawnPlayer(int spawnLocation)
      {
          string spawnPointName = SceneManager.GetActiveScene().name
              + "_" + spawnLocation;

          //Find where character should be spawned in current scene
          Transform spawnPointTransform =
              GameObject.Find(spawnPointName).GetComponent<Transform>();
          //Create Character

          Instantiate(playerPrefab, spawnPointTransform.position,
              spawnPointTransform.rotation);
      }
      //provides access to private variable _instance
      //-variable must be declared above
      //-variable must be static
      */
    public void StartGame()
    {
        SceneManager.LoadScene("ContraLevel");
    }

    public void QuitGame()
    {
        Debug.Log("Quiting Game...");
        Application.Quit();
    }
    public static GameManager instance
    {
        get { return _instance;}
        set { _instance = value; }
    }

    //Declare variable 'scrore' and create get/set
    public int score
    {
        get;set;
    }
    public int life
    {
        get; set;
    }
}
