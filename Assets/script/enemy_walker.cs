﻿using UnityEngine;
using System.Collections;

public class enemy_walker : MonoBehaviour {


    Rigidbody2D rb;


    public Rigidbody2D rb2;


    public float speed;

    public bool isFacingLeft;

    // Use this for initialization
    void Start () {

        rb = GetComponent<Rigidbody2D>();

        if (!rb)
        {

            Debug.LogWarning("rb: No Rigidbody2D found on GameObject.");
        }



        if (speed <= 0.9f)
        {

            Debug.LogWarning("Speed variable not set. Setting to default value 5.");


            speed = 1.0f;
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (isFacingLeft)
            rb.velocity = new Vector2(-speed, rb.velocity.y);
        else
            rb.velocity = new Vector2(speed, rb.velocity.y);
	
	}

    void OnTriggerEnter2D(Collider2D c)
    {
        if(c.gameObject.tag=="enemyCollider")
        {
            flip();
        }
    }

    void flip()
    {

        isFacingLeft = !isFacingLeft;


        Vector3 scaleFactor = transform.localScale;

        scaleFactor.x *= -1;

        transform.localScale = scaleFactor;

    }
}
