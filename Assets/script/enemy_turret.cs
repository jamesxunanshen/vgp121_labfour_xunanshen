﻿using UnityEngine;
using System.Collections;

public class enemy_turret : MonoBehaviour
{

    public Transform projectileSpawnPoint;

    public Transform target;

    float distance;

    public float range;

    float relativeDistance;

    public bool isFacingLeft;

    public Projectile projectile;

    public float projectileFireRate;
    float timeSinceLastFire = 0.0f;

    // Use this for initialization
    void Start()
    {
        if (projectileFireRate <= 0)
        {
            projectileFireRate = 0.2f;
        }
        if (range <= 0)
        {
            range = 2.00f;
        }
    }

    // Update is called once per frame
    void Update()
    {
        distance = Vector2.Distance(transform.position, target.position);

        relativeDistance = transform.position.x - target.position.x;


        if (Time.time > timeSinceLastFire + projectileFireRate & distance < range)
        {

            Projectile pTemp = Instantiate(projectile, projectileSpawnPoint.position, projectileSpawnPoint.rotation) as Projectile;
            if (!isFacingLeft)
                pTemp.GetComponent<Projectile>().speed = 2;
            else if (isFacingLeft)
                pTemp.GetComponent<Projectile>().speed = -2;
            timeSinceLastFire = Time.time;
        }

        if (relativeDistance < 0 && isFacingLeft)
        {
            flip();
        }
        else if (relativeDistance > 0 && !isFacingLeft)
        { flip(); }


    }


    void flip()
    {

        isFacingLeft = !isFacingLeft;


        Vector3 scaleFactor = transform.localScale;

        scaleFactor.x *= -1;

        transform.localScale = scaleFactor;

    }

}