﻿using UnityEngine;
using System.Collections;

public class randomSpawn : MonoBehaviour {

    public Transform teleport;
    public GameObject[] prefeb;

    void Start()
    { 
        
        int prefeb_num = Random.Range(0, 2);

        Instantiate(prefeb[prefeb_num], teleport.position, teleport.rotation);

    }
}
